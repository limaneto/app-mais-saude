import React from 'react'
import { Actions, ActionConst, Scene } from 'react-native-router-flux'
import { Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'


// Scene component imports
import App from './containers/App'
import CategoryList from './containers/CategoryList'
import DoctorDetails from './containers/DoctorDetails'
import DoctorsList from './containers/DoctorsList'
import FirstRun from './containers/FirstRun'
import Login from './containers/Login'
import NavSearchButton from './components/NavSearchButton'
import Payment from './containers/Payment'
import PaymentFinish from './containers/PaymentFinish'
import PhoneNumber from './containers/PhoneNumber'
import PhoneNumberConfirm from './containers/PhoneNumberConfirm'
import Register from './containers/Register'
import SearchCategory from './containers/SearchCategory'
import SelectDate from './containers/SelectDate'
import Start from './containers/Start'
import SplashScreen from './components/SplashScreen'

const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{color: selected ? 'blue' : 'black' }}>{title}</Text>
  )
}

const scenes = [{
  key: 'app',
  title: 'Main Screen',
  component: App,
  icon: TabIcon,
  renderLeftButton: () => <Icon name="menu" onPress={() => { console.log('Left') }}/>
}, {
  key: 'doctorDetails',
  title: 'Agendar',
  hideNavBar: false,
  component: DoctorDetails,
  leftButton: props => (<View />),
  renderRightButton: props => (<NavSearchButton />)
}, {
  key: 'doctorsList',
  title: 'Category Title',
  getTitle: (props) => props.title,
  hideNavBar: false,
  component: DoctorsList,
  leftButton: props => (<View />),
  renderRightButton: props => (<NavSearchButton />)
}, {
  key: 'firstRun',
  title: 'Tutorial',
  component: FirstRun,
  type: ActionConst.RESET,
  icon: TabIcon,
  hideNavBar: true
}, {
  key: 'start',
  title: 'Facebook Login',
  component: Start,
  type: ActionConst.PUSH,
  icon: TabIcon,
  hideNavBar: false
}, {
  key: 'payment',
  title: 'Realizar Pagamento',
  hideNavBar: false,
  component: Payment
}, {
  key: 'paymentFinish',
  title: 'Realizar Pagamento',
  hideNavBar: true,
  component: PaymentFinish
}, {
  key: 'phoneNumber',
  title: 'Número de Celular',
  hideNavBar: true,
  component: PhoneNumber
}, {
  key: 'phoneNumberConfirm',
  title: 'Confirmar Número',
  hideNavBar: true,
  component: PhoneNumberConfirm
}, {
  key: 'login',
  title: 'Login',
  hideNavBar: true,
  component: Login
}, {
  key: 'searchCategory',
  title: 'Agendar Consulta',
  type: ActionConst.RESET,
  hideNavBar: false,
  renderBackButton: false,
  component: SearchCategory,
  leftButton: props => (<View />),
  renderRightButton: props => (<NavSearchButton />)
}, {
  key: 'selectDate',
  title: 'Selecionar Data',
  hideNavBar: false,
  component: SelectDate,
  leftButton: props => (<View />),
  renderRightButton: props => (<NavSearchButton />)
}, {
  key: 'splash',
  title: 'Splash Screen',
  initial: true,
  hideNavBar: true,
  component: SplashScreen
}, {
  key: 'register',
  title: 'Cadastro',
  hideNavBar: true,
  component: Register
}]

function scenesToJSX (scenes) {
  return scenes.map(scene => {
    let children = scene.children && scene.children.length ? (
      scene.children.map(childScene => (<Scene {...childScene}/>))) : (
      [])

    if (children.length) {
      return (
        <Scene {...scene}>
          { children }
        </Scene>
      )
    } else {
      return <Scene {...scene}/>
    }
  })
}

export default scenesToJSX(scenes)
