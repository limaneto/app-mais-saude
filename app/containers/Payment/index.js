import React from 'react'
import { Image, Text, TextInput, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { Actions } from 'react-native-router-flux'

import B from '../../components/B'
import DetailsFooter from '../DetailsFooter'

import styles from './styles'

class Payment extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  render () {
    return (
      <View style={styles.content}>
        <View style={styles.body}>
          <View style={styles.infoBox}>
            <B>Dr. José Cauby Lima de Andrade Neto</B>
            <Text style={styles.infoBoxText}>Cardiologista</Text>
            <Text style={styles.infoBoxText}>Data: 16 Julho de 2017 às 18:00</Text>
            <Text style={styles.infoBoxText}>Av. Desembargador Moreira 2001, Sala 1103</Text>
          </View>
          <View style={styles.infoValueBox}>
            <Text style={styles.infoValueText}>Valor da Consulta</Text>
            <B style={styles.infoValueText}>R$ 700,00</B>
          </View>
          <View style={styles.formWrapper}>
            <View style={styles.inputWrapper}>
              <B>Nome</B>
              <TextInput style={styles.inputControl} placeholder="(como no cartão)" underlineColorAndroid='transparent'/>
            </View>
            <View style={styles.inputWrapper}>
              <B>Número do Cartão</B>
              <TextInput style={styles.inputControl} placeholder="(apenas números)" underlineColorAndroid='transparent'/>
            </View>
            <View style={styles.inputWrapper}>
              <B>Data de Validade</B>
              <TextInput style={styles.inputControl} placeholder="(MM/AAAA)" underlineColorAndroid='transparent'/>
            </View>
            <View style={{
              flexDirection: 'row',
              alignItems: 'center'
            }}>
              <View style={styles.inputWrapper}>
                <B>CCV</B>
                <TextInput style={styles.inputControl} placeholder="(código de seg)" underlineColorAndroid='transparent'/>
              </View>
              <Image style={{marginLeft: 30}}
                height={30} width={46}
                source={require('../../../images/assets/icon_ccv.jpg')} />
            </View>
          </View>
        </View>
        <DetailsFooter buttonText="Confirmar Pagamento" buttonPress={Actions.paymentFinish}/>
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
})

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Payment)
