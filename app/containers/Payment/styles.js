import globalStyles from '../../global-styles'

export default {
  ...globalStyles,
  content: {
    ...globalStyles.content,
    backgroundColor: '#E9EBEE',
    paddingHorizontal: 0,
    paddingBottom: 0
  },
  body: {
    ...globalStyles.content,
    paddingTop: 20,
    paddingHorizontal: 40,
    justifyContent: 'flex-start',
    flex: 7
  },
  infoBox: {
    backgroundColor: '#FFFFFF',
    marginTop: 25,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  infoBoxText: {
    fontSize: 12,
    paddingVertical: 2
  },
  infoValueBox: {
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20
  },
  infoValueText: {
    fontSize: 18
  },
  formWrapper: {
    flex: 1
  },
  inputWrapper: {
    backgroundColor: '#D5D5D5',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginVertical: 2,
    height: 35
  },
  inputControl: {
    marginLeft: 5,
    fontSize: 14,
    minWidth: 120
  }
}
