import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

import BlueGradientButton from '../../components/BlueGradientButton'

import styles from './styles'

class DetailsFooter extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  render () {
    return (
      <View style={styles.footer}>
        <Text style={styles.priceText}>R$ 700,00</Text>
        <BlueGradientButton style={styles.selectDateButton} onPress={this.props.buttonPress}>
          <Text style={styles.selectDateBtnText}>
            {this.props.buttonText}
          </Text>
          <Icon name='arrow-forward' size={20} color='white' style={styles.selectDateBtnIcon}/>
        </BlueGradientButton>
      </View>
    )
  }
}

export default DetailsFooter
