export default {
  footer: {
    height: 70,
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 25,
    paddingVertical: 20
  },
  priceText: {
    flex: 2,
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#139E97'
  },
  selectDateButton: {
    flex: 4,
    borderRadius: 99,
    height: 45
  },
  selectDateBtnText: {
    color: 'white',
    marginRight: 20,
    fontWeight: 'bold'
  },
  selectDateBtnIcon: {
    position: 'absolute',
    top: 12,
    right: 15
  }
}
