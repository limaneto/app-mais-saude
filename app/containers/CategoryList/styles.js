export default {
  content: {
    flex: 1,
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  keyboardAvoid: {
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20
  },
  location: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#dedede',
    backgroundColor: '#FFF'
  },
  locationText: {
    fontWeight: 'bold'
  },
  locationChangeBtn: {
  },
  locationChangeBtnText: {
    color: '#0D8D87',
    fontWeight: 'bold'
  },
  resultsList: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch'
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
  },
  footerButton: {
    alignItems: 'center'
  },
}
