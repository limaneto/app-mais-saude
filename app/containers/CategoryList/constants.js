export const OPEN_CATEGORY = 'app/SearchCategory/OPEN_CATEGORY'

export const SET_LIST_FILTER = 'app/SearchCategory/SET_LIST_FILTER'

export const GET_DEVICE_LOCATION = 'app/SearchCategory/GET_DEVICE_LOCATION'
export const GET_NEARBY_SEARCH = 'app/SearchCategory/GET_NEARBY_SEARCH'

export const GET_ESPEL_LIST = 'app/SearchCategory/GET_ESPEL_LIST'

export const SET_ESPEL_LIST = 'app/SearchCategory/SET_ESPEL_LIST'
