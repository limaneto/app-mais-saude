import { createSelector } from 'reselect'

/**
 * Direct selector to the categoryList state domain
 */
const searchCategoryDomain = () => state => state.get('search').toJS()

/**
 * Other specific selectors
 */
const routeSelector = () => state => state.get('route').toJS()

const categoryListSelector = () => state => {
  let result = state.get('search').toJS().results
  let filter = state.get('search').toJS().filter

  result = result.filter(result => {
    return result.nome_espel.toLowerCase().includes(filter.text.toLowerCase())
  })

  return result
}

/**
 * Default selector used by CategoryList
 */
const makeSelectCategoryList = () => createSelector(
  searchCategoryDomain(),
  substate => substate.toJS()
)

export default makeSelectCategoryList
export {
  routeSelector,
  categoryListSelector,
  searchCategoryDomain
}
