import React from 'react'
import { ActivityIndicator, ListView, Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import B from '../../components/B'
import CategoryRow from '../../components/CategoryRow'
import SectionHeader from './SectionHeader'

import {
  getNearbySearch,
  getEspelList,
  setEspelList
} from './actions'

import {
  getDoctorsList
} from '../DoctorsList/actions'

import {
  categoryListSelector,
  searchCategoryDomain,
  routeSelector
} from './selectors'

import styles from './styles'

class CategoryList extends React.Component {
  componentWillMount () {
    console.log('==> Fetching device location.')

    // Getting Fortaleza as Location
    let location = {
      coords: {
        latitude: -3.7913486,
        longitude: -38.5893111
      }
    }

    this.props.dispatch(getNearbySearch({
      location: `${location.coords.latitude},${location.coords.longitude}`,
      types: ['regions'],
      radius: 5000
    }))

    this.props.fetchSearch()

  /*
    navigator.geolocation
      .getCurrentPosition(location => {
        this.props.dispatch(getNearbySearch({
          location: `${location.coords.latitude},${location.coords.longitude}`,
          types: ['regions'],
          radius: 5000
        }))
        .then(({action, value}) => {
          console.log('Fulfilled: ', action, value)
        })
        .catch(err => {
          console.log(err)
        })
      }, err => {
        console.log('==> Retry to get location...')
        console.log(err)
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000})
    */
  }

  fetchDataSource () {
    const { fetchSearch, setListData } = this.props
    const createDataSource = this.createDataSource.bind(this)
    fetchSearch()
      .then(response => setListData(createDataSource(response.value.data)))
      .catch(err => {
        console.log(err)
      })
  }

  createDataSource (data) {
    const getSectionData = (dataBlob, sectionId) => dataBlob[sectionId]
    const getRowData = (dataBlob, sectionId, rowId) => dataBlob[`${rowId}`]

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
      getSectionData,
      getRowData
    })

    const { dataBlob, sectionIds, rowIds } = this.formatData(data)
    return ds.cloneWithRowsAndSections(dataBlob, sectionIds, rowIds)
  }

  formatData (source) {
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')

    const dataBlob = {}
    const sectionIds = []
    const rowIds = []

    for (let sectionId = 0; sectionId < alphabet.length; sectionId++) {
      const currentChar = alphabet[sectionId]
      const users = source.filter(category => category.nome_espel.toUpperCase().indexOf(currentChar) === 0)

      if (users.length > 0) {
        sectionIds.push(sectionId)
        dataBlob[sectionId] = { character: currentChar }
        rowIds.push([])

        for (let i = 0; i < users.length; i++) {
          const rowId = `${sectionId}:${i}`
          rowIds[rowIds.length - 1].push(rowId)
          dataBlob[rowId] = users[i]
        }
      }
    }

    return { dataBlob, sectionIds, rowIds }
  }

  renderCategoryRow (data) {
    const pressEvent = this.handleListItemPress.bind(this, data)
    return (
      <CategoryRow {...data} onPress={pressEvent}/>
    )
  }

  renderSectionHeader (data) {
    return (<SectionHeader {...data} />)
  }

  renderListView (data, loading) {
    if (loading.pending) {
      return <ActivityIndicator color={styles.locationChangeBtnText.color}
        animating={true} style={styles.loading}/>
    }
    if (loading.fulfilled) {
      return (<ListView dataSource={data}
              renderRow={this.renderCategoryRow.bind(this)}
              renderSectionHeader={this.renderSectionHeader} />)
    }
    if (loading.rejected) {
      return (
        <View style={styles.content}>
          <B>Um Erro Ocorreu</B>
          <Text>Tente novamente mais tarde</Text>
        </View>
      )
    }

    return (<View style={styles.content}/>)
  }

  handleListItemPress (category) {
    this.props.loadDoctorsList(category.pk)
    Actions.doctorsList({
      title: category.nome_espel
    })
  }

  render () {
    const { loading } = this.props.search
    const { categoryList } = this.props
    const createDataSource = this.createDataSource.bind(this)

    return this.renderListView(createDataSource(categoryList), loading)
  }
}

const mapStateToProps = createStructuredSelector({
  search: searchCategoryDomain(),
  categoryList: categoryListSelector(),
  route: routeSelector()
})

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    fetchSearch: () => dispatch(getEspelList()),
    setListData: data => dispatch(setEspelList(data)),
    loadDoctorsList: espel => dispatch(getDoctorsList(espel))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryList)
