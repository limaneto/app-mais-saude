import axios from 'axios'
import {
  GET_NEARBY_SEARCH,
  GET_ESPEL_LIST,
  OPEN_CATEGORY,
  SET_ESPEL_LIST,
  SET_LIST_FILTER
} from './constants'

import { api, googlePlacesAPI } from '../../config'

export function openCategory (category) {
  return {
    type: OPEN_CATEGORY,
    payload: category
  }
}

export function filterSearch (payload) {
  return {
    type: SET_LIST_FILTER,
    payload
  }
}

export function getNearbySearch (options) {
  const req = googlePlacesAPI.get(options)
  return {
    type: GET_NEARBY_SEARCH,
    payload: axios(req)
  }
}

export function getEspelList () {
  return {
    type: GET_ESPEL_LIST,
    payload: axios(api.getEspelList())
  }
}

export function setEspelList (dataSource) {
  return {
    type: SET_ESPEL_LIST,
    payload: dataSource
  }
}
