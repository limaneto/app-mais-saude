import { fromJS } from 'immutable'

import {
  GET_ESPEL_LIST,
  GET_NEARBY_SEARCH,
  OPEN_CATEGORY,
  SET_ESPEL_LIST,
  SET_LIST_FILTER
} from './constants'

const initialState = fromJS({
  results: [],
  category: {},
  filter: {
    text: ''
  },
  locality: {},
  loading: {
    pending: false,
    fulfilled: false,
    rejected: false
  }
})

function categoryListReducer (state = initialState, action) {
  const {type, payload} = action
  switch (type) {
    case OPEN_CATEGORY:
      return state.set('category', fromJS(payload))

    case `${GET_NEARBY_SEARCH}_PENDING`:
      let loading = state.get('loading').set('pending', true)
      return state.set('loading', loading)

    case `${GET_NEARBY_SEARCH}_FULFILLED`:
      return state.set('loading', fromJS({
        pending: false,
        fulfilled: true,
        rejected: false
      })).set('locality', fromJS({
        name: payload.data.results[0].name,
        latitude: payload.data.results[0].latitude,
        longitude: payload.data.results[0].longitude
      }))

    case GET_ESPEL_LIST + '_PENDING':
      return state.set('loading', fromJS({
        pending: true,
        filfilled: false,
        rejected: false
      }))

    case GET_ESPEL_LIST + '_FULFILLED':
      return state.set('loading', fromJS({
        pending: false,
        filfilled: true,
        rejected: false
      })).set('results', fromJS(payload.data))

    case GET_ESPEL_LIST + '_REJECTED':
      return state.set('loading', fromJS({
        pending: false,
        filfilled: false,
        rejected: true
      }))

    case SET_ESPEL_LIST:
      return state.set('dataSource', fromJS(payload))

    case SET_LIST_FILTER:
      return state.set('filter', fromJS(payload))

    default:
      return state
  }
}

export default categoryListReducer
