import React from 'react'
import styled from 'styled-components/native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const Wrapper = styled.View`
  flex: 1;
  flex-direction: row;
  align-self: stretch;
  justify-content: space-between;
  padding-horizontal: 5;
`

const View = styled.View`
  flex: 1;
  padding-right: 1;
  padding-vertical: 10;
  border-bottom-width: 0.2;
  border-bottom-color: rgba(192, 192, 192, 0.5);
  background-color: #FFF;
  flex-direction: row;
  align-items: center;
  elevation: 5;
`

const Text = styled.Text`
  margin-left: 12;
  font-size: 16;
  font-weight: bold;
  align-self: flex-start;
`

const iconStyle = {
  alignSelf: 'flex-end',
  alignContent: 'flex-end',
  fontWeight: 'bold'
}

const SectionHeader = (props) => (
  <View>
    <Wrapper>
      <Text>{props.character}</Text>
      <Icon name='filter-variant' size={24} style={iconStyle}/>
    </Wrapper>
  </View>
)

export default SectionHeader
