import React from 'react'
import {
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import CategoryList from '../CategoryList'
import TabFooter from '../../components/TabFooter'

/* Actions */
import {
  filterSearch
} from '../CategoryList/actions'

/* Selectors */
import { searchCategoryDomain, routeSelector } from './selectors'

import styles from './styles'

class SearchCategory extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  handleSearchChange (text) {
    this.props.filterSearch({
      text
    })
  }

  renderLocalityName (locality) {
    if (locality.name) {
      return (<Text style={styles.locationText} >{locality.name} </Text>)
    } else {
      return (<Text style={styles.locationText} >Obtendo localização...</Text>)
    }
  }

  renderSearchBar () {
    const { search } = this.props.route
    if (search.on) {
      return (
        <View style={styles.search.wrapper}>
          <TextInput style={styles.search.input}
            multiline={false}
            underlineColorAndroid='transparent'
            placeholderTextColor='#aeaeae'
            placeholder='Buscar'
            onChangeText={this.handleSearchChange.bind(this)}/>
        </View>
      )
    } else {
      return (<View />)
    }
  }

  render () {
    const { locality } = this.props.search

    return (
      <View style={styles.content}>
        <View style={styles.location}>
          {this.renderLocalityName(locality)}
          <TouchableOpacity>
            <Text style={styles.locationChangeBtnText}>ALTERAR</Text>
          </TouchableOpacity>
        </View>
        {this.renderSearchBar.bind(this)()}
        <CategoryList />
        <TabFooter />
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  search: searchCategoryDomain(),
  route: routeSelector()
})

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    filterSearch: text => dispatch(filterSearch(text))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchCategory)
