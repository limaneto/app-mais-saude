export default {
  content: {
    flex: 1,
    marginTop: 50
  },
  keyboardAvoid: {
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20
  },
  location: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#dedede',
    backgroundColor: '#FFF'
  },
  locationText: {
    fontWeight: 'bold'
  },
  locationChangeBtn: {
  },
  locationChangeBtnText: {
    color: '#0D8D87',
    fontWeight: 'normal',
    fontSize: 12
  },
  resultsList: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch'
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
  },
  footerButton: {
    alignItems: 'center'
  },
  search: {
    wrapper: {
      borderBottomColor: '#dedede',
      paddingHorizontal: 10,
      marginVertical: 5
    },
    input: {
      backgroundColor: '#FFF',
      borderWidth: 1,
      borderColor: '#dedede',
      borderRadius: 25,
      color: '#aaa',
      fontSize: 14,
      paddingHorizontal: 10,
      height: 40
    }
  }
}
