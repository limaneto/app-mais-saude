import React from 'react'
import { View, Text, Image } from 'react-native'
import { Actions } from 'react-native-router-flux'

import B from '../../components/B'
import Input from '../../components/Input'
import Logo from '../../components/Logo'
import Small from '../../components/Small'
import GradientButton from '../../components/GradientButton'

import styles from './styles'

export default class PhoneNumber extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  render() {
    return (
      <View style={styles.content}>
        <View>
          <Logo source={require('../../../images/logo.png')} />
        </View>
        <View>
          <Input icon='ios-phone-portrait'
            placeholder="Número de Celular"
            keyboardType='phone-pad'
          />
          <GradientButton onPress={Actions.phoneNumberConfirm}>
            <B style={{color: 'white', fontSize: 18}}>Continuar</B>
          </GradientButton>
        </View>
        <View>
          <Small>
            Já tem uma conta? <B onPress={Actions.login}>Entre Aqui</B>
          </Small>
        </View>
      </View>
    )
  }
}
