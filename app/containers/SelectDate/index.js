import React from 'react'
import moment from 'moment'
import {
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import Calendar from 'react-native-calendar'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import B from '../../components/B'
import H3 from '../../components/H3'

import DetailsFooter from '../../containers/DetailsFooter'

import styles from './styles'

import { brazilianDayHeadings, brazilianMonths } from './constants.js'

class SelectDate extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  render () {
    const today = moment().format('YYYY-MM-DD')
    return (
      <View style={styles.content}>
        <View style={styles.body}>
          <View style={styles.title}>
            <B style={styles.titleDrName}>Dr. José Cauby de Andrade Neto</B>
            <B style={styles.titleDrSpecialty}>Cardiologista</B>
          </View>
          <View style={styles.calendarWrapper}>
            <Calendar customStyle={styles.calendarStyle}
              today={today}
              startDate={today}
              showControls={true} monthNames={brazilianMonths}
              dayHeadings={brazilianDayHeadings}
              nextButtonText={'>'} prevButtonText={'<'}
            />
          </View>
          <View style={styles.selectHourWrapper}>
            <H3>Selecionar Horário</H3>
            <View style={styles.selectHourButtonGroup}>
              <TouchableOpacity style={styles.selectHourButton.default}>
                <Text style={styles.selectHourButtonText.default}>8:30h</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.selectHourButton.active}>
                <Text style={styles.selectHourButtonText.active}>10:00h</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.selectHourButton.default}>
                <Text style={styles.selectHourButtonText.default}>14:15h</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.selectHourButton.default}>
                <Text style={styles.selectHourButtonText.default}>17:45h</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <DetailsFooter buttonText="Realizar Pagamento" buttonPress={Actions.payment}/>
      </View>
    )
  }
}

export default SelectDate
