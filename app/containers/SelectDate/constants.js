export const brazilianMonths = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro'
]

export const brazilianDayHeadings = [
  'D', 'S', 'T', 'Q', 'Q', 'S', 'S'
]
