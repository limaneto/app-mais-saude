import globalStyles from '../../global-styles.js'

const activeColor = '#62B7B4'
const calendarFontSize = 12
export default {
  ...globalStyles,
  content: {
    ...globalStyles.content,
    flex: 1,
    backgroundColor: '#E9EBEE',
    paddingTop: 50,
    paddingBottom: 0,
    paddingHorizontal: 0
  },
  body: {
    flex: 1,
    paddingHorizontal: 20,
    marginBottom: 50
  },
  calendarWrapper: {
    backgroundColor: 'white',
    borderColor: activeColor,
    marginHorizontal: 10
  },
  calendarStyle: {
    calendarContainer: {
      borderColor: activeColor,
      borderWidth: 1,
      padding: 10,
      height: 270
    },
    calendarHeading: {
      borderTopWidth: 0,
      borderBottomWidth: 0
    },
    controlButtonText: {
      fontWeight: '900',
      fontSize: 20,
      color: activeColor
    },
    currentDayCircle: {
      backgroundColor: activeColor
    },
    day: {
      fontSize: calendarFontSize,
      fontWeight: '700'
    },
    dayButton: {
      paddingHorizontal: 0,
      paddingVertical: 0,
      borderTopWidth: 0,
      borderBottomWidth: 0,
      width: 40
    },
    dayButtonFiller: {
      paddingHorizontal: 0,
      paddingVertical: 0,
      borderTopWidth: 0,
      borderBottomWidth: 0,
      backgroundColor: 'transparent',
      width: 40
    },
    dayHeading: {
      color: activeColor,
      fontWeight: '700',
      fontSize: calendarFontSize,
      paddingHorizontal: 0,
      paddingVertical: 0
    },
    selectedDayCircle: {
      backgroundColor: activeColor
    },
    title: {
      color: activeColor,
      fontSize: 14,
      fontWeight: '700'
    },
    weekendDayButton: {
      paddingHorizontal: 0,
      paddingVertical: 0,
      borderTopWidth: 0,
      borderBottomWidth: 0,
      backgroundColor: 'transparent',
      width: 40
    },
    weekendHeading: {
      fontWeight: '700',
      color: activeColor,
      fontSize: calendarFontSize
    }
  },
  selectHourWrapper: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  selectHourButton: {
    default: {
      alignSelf: 'stretch',
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 1,
      borderColor: activeColor,
      backgroundColor: '#FFF',
      paddingVertical: 5,
      flex: 1,
      width: 50,
      height: 35
    },
    active: {
      alignSelf: 'stretch',
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 1,
      borderColor: activeColor,
      backgroundColor: activeColor,
      paddingVertical: 5,
      flex: 1,
      width: 50,
      height: 35
    }
  },
  selectHourButtonGroup: {
    flexDirection: 'row',
    flex: 1,
    paddingVertical: 10
  },
  selectHourButtonText: {
    default: {
      fontSize: 12,
      fontWeight: '700',
      color: activeColor
    },
    active: {
      fontSize: 12,
      fontWeight: '700',
      color: '#FFF'
    }
  },
  title: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5
  },
  titleDrName: {
    fontSize: 16,
    color: activeColor
  },
  titleDrSpecialty: {
    fontSize: calendarFontSize
  }
}
