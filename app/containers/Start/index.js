import React from 'react'
import { Content, Text } from 'native-base'
import styles from './styles'
export default class Start extends React.Component {
  render() {
    return (
      <Content contentContainerStyle={styles.content}>
        <Text>Hellow World!</Text>
      </Content>
    )
  }
}
