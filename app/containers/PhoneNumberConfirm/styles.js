import globalStyles from '../../global-styles'

export default {
  ...globalStyles,
  content: {
    ...globalStyles.content,
    alignItems: 'stretch'
  }
}
