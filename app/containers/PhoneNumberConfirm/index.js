import React from 'react'
import { View, Text, Image } from 'react-native'
import { Actions } from 'react-native-router-flux'

import B from '../../components/B'
import Input from '../../components/Input'
import Logo from '../../components/Logo'
import Small from '../../components/Small'
import GradientButton from '../../components/GradientButton'
import OutlineButton from '../../components/OutlineButton'

import styles from './styles'

export default class PhoneNumber extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  render() {
    return (
      <View style={styles.content}>
        <View>
          <Logo source={require('../../../images/logo.png')} />
        </View>
        <View >
          <Text style={styles.featureText}>
            Dentro de alguns instantes você receberáum SMS com um código de confirmação.
          </Text>
          <Input
            style={styles.featureText}
            placeholder="·  ·  ·  ·  ·  ·  ·"
            keyboardType='phone-pad'
          />
          <GradientButton onPress={Actions.register}>
            <B style={{color: 'white', fontSize: 18}}>Confirmar</B>
          </GradientButton>
          <View style={{marginTop: 30}}>
            <OutlineButton>
              <B style={{color: '#A7A7A7'}}>REENVIAR CÓDIGO</B>
            </OutlineButton>
            <OutlineButton onPress={Actions.pop}>
              <B style={{color: '#A7A7A7'}}>ALTERAR NÚMERO</B>
            </OutlineButton>
          </View>
        </View>
        <View>
          <Small>
            Já tem uma conta? <B onPress={Actions.login}>Entre Aqui</B>
          </Small>
        </View>
      </View>
    )
  }
}
