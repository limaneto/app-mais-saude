import axios from 'axios'
import {
  api
} from '../../config'

import {
  GET_DOCTOR_LIST,
  OPEN_DOCTOR_DETAILS
} from './constants'

export function getDoctorsList (espel) {
  const payload = axios(api.getDoctorsListByEspel(espel))
  return {
    type: GET_DOCTOR_LIST,
    payload
  }
}

export function openDoctorDetails (payload) {
  return {
    type: OPEN_DOCTOR_DETAILS,
    payload
  }
}
