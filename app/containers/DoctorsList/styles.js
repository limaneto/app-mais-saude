import globalStyles from '../../global-styles'

export default {
  ...globalStyles,
  content: {
    flex: 1,
    marginTop: 50,
    backgroundColor: '#E9EBEE',
    justifyContent: 'space-between'
  },
  location: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#dedede',
    backgroundColor: '#FFF'
  },
  locationText: {
    fontWeight: 'bold'
  },
  locationChangeBtn: {
  },
  locationChangeBtnText: {
    color: '#0D8D87',
    fontWeight: 'bold'
  },
  doctorListItem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    marginBottom: 20,
    paddingVertical: 20,
    paddingHorizontal: 40
  },
  doctorPhoto: {
    width: 80,
    height: 80,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  doctorInfo: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 20
  },
  doctorName: {
    color: '#32B5B3',
    fontSize: 18,
    fontWeight: 'bold'
  },
  doctorSpecialty: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16
  },
  doctorCRM: {
    color: '#A8A8A8'
  },
  doctorPrice: {
    color: '#32B5B3',
    fontWeight: 'bold'
  }
}
