import { createSelector } from 'reselect'

/**
 * Direct selector to the categoryList state domain
 */
const searchCategoryDomain = () => state => state.get('search').toJS()

/**
 * Other specific selectors
 */
const routeSelector = () => state => state.get('route').toJS()
const categoryDomain = () => state => state.get('search').get('category').toJS()
const doctorListDomain = () => state => state.get('doctorsList').get('list').toJS()
const localityDomain = () => state => state.get('search').get('locality').toJS()

/**
 * Default selector used by CategoryList
 */
const makeSelectCategoryList = () => createSelector(
  searchCategoryDomain(),
  substate => substate.toJS()
)

export default makeSelectCategoryList
export {
  routeSelector,
  searchCategoryDomain,
  categoryDomain,
  doctorListDomain,
  localityDomain
}
