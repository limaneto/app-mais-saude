import React from 'react'
import { Image, FlatList, Text, TouchableOpacity, View } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import TabFooter from '../../components/TabFooter'

import { getDoctorsList, openDoctorDetails } from './actions'
import {
  categoryDomain,
  doctorListDomain,
  localityDomain,
  routeSelector
} from './selectors'

import styles from './styles'

class DoctorsList extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  componentWillMount () {
  }

  handleListPress (doctor) {
    Actions.doctorDetails({
      doctor
    })
  }

  renderListItem ({item, index}) {
    return (
      <TouchableOpacity key={index} style={styles.doctorListItem} onPress={this.handleListPress.bind(this, item)}>
        <Image style={styles.doctorPhoto} source={require('../../../etc/thumbnail/doctor_thumb.png')} />
        <View style={styles.doctorInfo}>
          <Text style={styles.doctorName}>{item.nome_especialista}</Text>
          <Text style={styles.doctorSpecialty}>{this.props.route.scene.title}</Text>
          <Text style={styles.doctorCRM}>{item.crm_especialista}</Text>
          <Text style={styles.doctorAddress}>{item.logradouro_clinica}, {item.numero_clinica}</Text>
          <Text style={styles.doctorPrice}>R$ {item.valor_clinica}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  renderLocalityName (locality) {
    if (locality.name) {
      return (<Text style={styles.locationText} >{locality.name} </Text>)
    } else {
      return (<Text style={styles.locationText} >Obtendo localização...</Text>)
    }
  }

  renderDoctorList (list) {
    return (
      <FlatList data={list} renderItem={this.renderListItem.bind(this)}/>
    )
  }

  render () {
    const { doctorList } = this.props
    return (
      <View style={styles.content}>
        <View style={styles.location}>
          {this.renderLocalityName(this.props.locality)}
          <TouchableOpacity>
            <Text style={styles.locationChangeBtnText}>Alterar</Text>
          </TouchableOpacity>
        </View>
        {this.renderDoctorList(doctorList)}
        <TabFooter />
      </View>
    )
  }
}
const mapStateToProps = createStructuredSelector({
  category: categoryDomain(),
  doctorList: doctorListDomain(),
  locality: localityDomain(),
  route: routeSelector()
})

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    openDoctorDetails: doctor => {
      dispatch(openDoctorDetails(doctor))
      Actions.doctorDetails()
    },
    loadDoctorsList: espel => {
      dispatch(getDoctorsList(espel))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DoctorsList)
