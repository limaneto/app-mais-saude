export const GET_DOCTOR_LIST = 'app/DoctorsList/GET_DOCTOR_LIST'

export const OPEN_DOCTOR_DETAILS = 'app/DoctorsList/OPEN_DOCTOR_DETAILS'
