import { fromJS } from 'immutable'

import {
  GET_DOCTOR_LIST,
  OPEN_DOCTOR_DETAILS
} from './constants'

const initialState = fromJS({
  doctor: {},
  list: [],
  loading: {
    pending: false,
    fulfilled: false,
    rejected: false
  }
})

function doctorsListDetails (state = initialState, action) {
  const {type, payload} = action

  switch (type) {
    case OPEN_DOCTOR_DETAILS:
      console.log('Abrindo detalhess....')
      return state.set('doctor', fromJS(payload))

    case GET_DOCTOR_LIST + '_PENDING':
      state.set('loading', fromJS({
        peding: true,
        fulfilled: false,
        rejected: false
      }))
      return state

    case GET_DOCTOR_LIST + '_FULFILLED':
      state.set('loading', fromJS({
        peding: false,
        fulfilled: true,
        rejected: false
      }))
      return state.set('list', fromJS(payload.data))

    case GET_DOCTOR_LIST + '_REJECTED':
      state.set('loading', fromJS({
        peding: false,
        fulfilled: false,
        rejected: true
      }))
      return state.set('list', fromJS([]))

    default:
      return state
  }
}

export default doctorsListDetails
