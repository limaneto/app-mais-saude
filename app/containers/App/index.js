import React from 'react'

/** Third-party Components **/
import {
  Button,
  Body,
  Container,
  Content,
  Header,
  Icon,
  Left,
  Text,
  Title
} from 'native-base'
import { Actions } from 'react-native-router-flux'

import styles from './styles'

/** Main Component **/
export default class App extends React.Component {
  render () {
    return (
      <Content contentContainerStyle={styles.content}> 
        <Text onPress={Actions.login}>Go to next View</Text>
      </Content>
    )
  }
}
