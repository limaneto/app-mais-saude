import globalStyles from '../../global-styles'

export default {
  ...globalStyles,
  featureText: {
    ...globalStyles.featureText,
    color: 'white',
    marginBottom: 50
  },
  content: {
    ...globalStyles.content,
    alignItems: 'stretch',
    paddingHorizontal: 25
  },
  carousel: {
    height: 410,
    width: 300,
    overflow: 'hidden'
  },
  carouselContent: {
    alignSelf: 'stretch',
    alignContent: 'stretch',
    justifyContent: 'center',
  },
  carouselPage: {
    flex: 1,
    alignSelf: 'stretch',
    alignContent: 'stretch',
    justifyContent: 'center',
  },
  card: {
    backgroundColor: 'rgba(255, 255, 255, 1)',
    borderRadius: 5,
    height: 350,
    paddingVertical: 20,
    paddingHorizontal: 10,
    marginHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  cardIcon: {
    width: 120,
    height: 120,
  },
  cardTitle: {
    textAlign: 'center',
    fontSize: 18,
    color: '#389BA1',
    marginVertical: 25,
  },
  pageInfoContainer: {
    marginVertical: 30
  },
  bulletStyle: {
    backgroundColor: 'rgba(255, 255, 255, 0.25)',
    borderWidth: 0
  },
  chosenBulletStyle: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)'
  },
  viewBg: {
    paddingHorizontal: 0,
    alignItems: 'center',
    alignSelf: 'stretch'
  },
  skipButton: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 42,
    marginBottom: 100,
    paddingHorizontal: 30
  },
  finishButton: {
    borderRadius: 25,
    borderWidth: 0.5,
    borderColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 42,
    marginBottom: 100,
    paddingHorizontal: 30
  }
}
