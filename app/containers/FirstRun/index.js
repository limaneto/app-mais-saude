import React from 'react'
import {
  View,
  Text,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Container } from 'native-base'
import Carousel from 'react-native-looped-carousel'

import B from '../../components/B'
import Input from '../../components/Input'
import Logo from '../../components/Logo'
import Small from '../../components/Small'
import GradientButton from '../../components/GradientButton'

import styles from './styles'

const { width, height } = Dimensions.get('window')

export default class FirstRun extends React.Component {
  constructor(props) {
    super(props)
    this.props = props
    const datasource = [{
      title: 'Precisa de um Médico?',
      description: 'Pesquise através das especialidades e encontre os melhores profissionais da sua região com desconto no valor original da consulta.',
      icon: require('../../../images/tutorial/Doctor.png')
    },{
      title: 'Marque sua Consulta',
      description: 'Selecione a melhor data e horário, confirme e realize o pagamento online com seu cartão de crédito. É super prático e seguro.',
      icon: require('../../../images/tutorial/Consulta.png')
    },{
      title: 'Marque seus Exames',
      description: 'Após a consulta você terá a opção de que nós façamos a marcação de seus exames, tudo de forma integrada com o aplicativo.',
      icon: require('../../../images/tutorial/Exame.png')
    }]

    this.state = {
      datasource,
      currentPage: 0
    }
  }

  renderCards(cards) {
    return cards.map((item, key) =>
      <Container style={styles.card} key={key}>
        <Image source={item.icon} style={styles.cardIcon} />
        <View style={{alignItems: 'center'}}>
          <B style={styles.cardTitle}>{item.title}</B>
          <Text style={{textAlign: 'center', paddingHorizontal: 15}}>
            {item.description}
          </Text>
        </View>
        <View style={styles.pageInfoContainer}>
          <Text>
            {this.state.currentPage + 1} / {this.state.datasource.length}
          </Text>
        </View>
      </Container>
    )
  }

  handleNextPage(page) {
    this.setState({
      ...this.state,
      currentPage: page
    })
  }

  render() {
    const { currentPage, datasource } = this.state
    return (
      <Image style={styles.viewBg}
        width={width} height={height}
        source={require('../../../images/assets/viewBg.png')}>

        <View style={styles.content}>
          <B style={{ ...styles.featureText, color: 'white'}}>
            Bem-vindo ao Mais Saúde Online
          </B>
          <Carousel
            ref={(carousel) => { this.carousel = carousel }}
            autoplay={false}
            currentPage={this.state.currentPage}
            onAnimateNextPage={this.handleNextPage.bind(this)}
            style={styles.carousel}
            contentContainerStyle={styles.carouselContent}
            pageStyle={styles.carouselPage}
            bullets
            bulletStyle={styles.bulletStyle}
            chosenBulletStyle={styles.chosenBulletStyle}
            bulletContainerStyle={{backgroundColor: 'rgba(0,0,0,0.25)'}}
            pageInfoBottomContainerStyle={styles.pageInfoContainer}
            pageInfoBackgroundColor='rgba(0,0,0,0)'
          >
            {this.renderCards(datasource)}
          </Carousel>
          { currentPage !== datasource.length - 1 ?
            <TouchableOpacity style={styles.skipButton} onPress={Actions.phoneNumber}>
              <Text style={{color: 'white', textAlign: 'center'}}>PULAR INTRODUÇÃO</Text>
            </TouchableOpacity>
            :
            <TouchableOpacity style={styles.finishButton} onPress={Actions.phoneNumber}>
              <Text style={{color: 'white', textAlign: 'center'}}>IR PARA O CADASTRO</Text>
            </TouchableOpacity>
          }
        </View>
      </Image>
    )
  }
}
