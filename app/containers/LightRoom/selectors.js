import { createSelector } from 'reselect'

const lightRoomDomain = () => state => state.get('doctorDetails').get('lightRoom').toJS()

const makeLightRoom = () => createSelector(
  lightRoomDomain(),
  substate => substate.toJS()
)

export default makeLightRoom

export {
  lightRoomDomain
}
