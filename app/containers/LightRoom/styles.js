import globalStyles from '../../global-styles'
export default {
  ...globalStyles,
  lightRoom: {
    wrapper: {
      flex: 1,
      backgroundColor: '#000',
      justifyContent: 'center'
    },
    image: {
      flex: 1,
      width: null,
      height: null
    }
  }
}
