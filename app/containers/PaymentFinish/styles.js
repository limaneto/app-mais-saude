import globalStyles from '../../global-styles'

export default {
  ...globalStyles,
  content: {
    ...globalStyles.content,
    backgroundColor: '#FFF',
    paddingHorizontal: 0,
    paddingBottom: 0
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  title: {
    color: '#79B249',
    fontSize: 24,
    textAlign: 'center'
  },
  smallText: {
    fontWeight: '700',
    textAlign: 'center'
  },
  button: {
    paddingVertical: 2,
    marginTop: 20
  },
  buttonText: {
    fontSize: 12
  }
}
