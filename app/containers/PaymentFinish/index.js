import React from 'react'
import { Image, Text, TextInput, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import B from '../../components/B'
import OutlineButton from '../../components/OutlineButton'
import TabFooter from '../../components/TabFooter'

import styles from './styles'

class PaymentFinish extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  render () {
    return (
      <View style={styles.content}>
        <View style={styles.body}>
          <Icon name='check-circle' size={150} color='#79B249'/>
          <B style={styles.title}>Sua solicitação foi enviada com sucesso!</B>
          <Text style={styles.smallText}>Estamos processando seu pagamento, em breve você receberá a confirmação.</Text>
          <OutlineButton style={styles.button}>
            <Text style={styles.buttonText}>IR PARA CONSULTAS</Text>
          </OutlineButton>
        </View>
        <TabFooter></TabFooter>
      </View>
    )
  }
}

export default PaymentFinish
