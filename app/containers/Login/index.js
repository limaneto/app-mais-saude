import React from 'react'
import { View } from 'react-native'
import { Actions } from 'react-native-router-flux'

import B from '../../components/B'
import Input from '../../components/Input'
import Logo from '../../components/Logo'
import Small from '../../components/Small'
import GradientButton from '../../components/GradientButton'
import FacebookButton from './FacebookButton'

import styles from './styles'
export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.props = props
  }

  render() {
    return (
      <View style={styles.content}>
        <View>
          <Logo source={require('../../../images/logo.png')} />
        </View>
        <Input placeholder={'E-mail'} icon="ios-at-outline"/>
        <Input placeholder={'Senha'} icon="ios-lock-outline" secureTextEntry={true} />
        <GradientButton onPress={Actions.searchCategory}>
          <B style={{color: 'white', fontSize: 18}}> Entrar </B>
        </GradientButton>
        <Small>Esqueceu a senha?</Small>
        <FacebookButton onPress={Actions.start}>
          <B style={{color: 'white', fontSize: 18}}> Entrar com Facebook</B>
        </FacebookButton>
        <Small>
          Não tem uma conta? <B onPress={Actions.phoneNumber}>Cadastre-se</B>
        </Small>
      </View>
    )
  }
}
