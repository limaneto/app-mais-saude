import styled from 'styled-components/native'

export default styled.TouchableOpacity`
  background-color: #5781AD;
  align-items: center;
  padding: 15 5;
  height: 52;
  border-radius: 25;
  margin: 10 0 0 0;
`
