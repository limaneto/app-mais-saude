import globalStyles from '../../global-styles'

export default {
  ...globalStyles,
  content: {
    ...globalStyles.content,
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingVertical: 20
  },
  keyboardAvoid: {
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20
  },
  buttonsView: {
    marginTop: 20
  }
}
