import React from 'react'
import { KeyboardAvoidingView, View, Text, Image } from 'react-native'
import { Actions } from 'react-native-router-flux'

import B from '../../components/B'
import Input from '../../components/Input'
import Logo from '../../components/Logo'
import Small from '../../components/Small'
import GradientButton from '../../components/GradientButton'
import FacebookButton from '../Login/FacebookButton'

import styles from './styles'
export default class Register extends React.Component {
  constructor(props) {
    super(props)
    this.props = props
  }

  render() {
    return (
      <View style={styles.content}>
        <KeyboardAvoidingView style={styles.keyboardAvoid} behavior="padding">
          <Logo source={require('../../../images/logo.png')}/>
          <Input placeholder='Nome e Sobrenome' icon='ios-contact' />
          <Input placeholder='E-mail' icon='ios-at-outline'/>
          <Input placeholder='Senha' icon='ios-lock-outline' secureTextEntry={true}/>
          <Input placeholder='Confirmar Senha' icon='ios-lock-outline' secureTextEntry={true}/>
        </KeyboardAvoidingView>
        <View style={styles.buttonsView}>
          <GradientButton style={{marginTop: -10}}>
            <B style={{color: 'white', fontSize: 18}}>Finalizar</B>
          </GradientButton>
          <FacebookButton>
            <B style={{color: 'white', fontSize: 18}}>Finalizar com Facebook</B>
          </FacebookButton>
          <Small>
            Já tem uma conta? <B onPress={Actions.login}>Entre Aqui</B>
          </Small>
        </View>
      </View>
    )
  }
}
