import React from 'react'
import { Actions } from 'react-native-router-flux'
import { Image, Modal, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { openBiography, openClinic } from './actions'
import { activeTabSelector } from './selectors'

import DetailsFooter from '../DetailsFooter'
import Biography from './Biography'
import Clinic from './Clinic'

import LightRoom from './LightRoom'

import styles from './styles'

class DoctorDetails extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  renderTabs (tab) {
    const { openBiography, openClinic } = this.props

    return (
      <View style={styles.tabWrapper}>
        <LightRoom />
        <TouchableOpacity
          style={tab === 'BIOGRAPHY' ? styles.tabSelected : styles.tab}
          onPress={openBiography}
        >
          <Text style={styles.tabText}>Sobre o Médico</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={tab === 'CLINIC' ? styles.tabSelected : styles.tab}
          onPress={openClinic}
        >
          <Text style={styles.tabText}>Sobre a Clínica</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderDetailsBody (tab) {
    if (tab === 'BIOGRAPHY') {
      return (<Biography />)
    } else {
      return (<Clinic />)
    }
  }

  handleTabChange (tab) {
    switch (tab) {
      case 'BIOGRAPHY':
        this.props.openBiography()
        break
      case 'CLINIC':
        this.props.openClinic()
        break
    }
  }

  render () {
    const renderTabs = this.renderTabs.bind(this)
    const renderDetailsBody = this.renderDetailsBody.bind(this)
    const { activeTab, doctor } = this.props

    return (
      <View style={styles.content}>
        <View style={styles.header}>
          <Image style={styles.doctorPhoto} source={require('../../../etc/thumbnail/doctor_thumb.png')} />
          <View style={styles.doctorInfo}>
            <Text style={styles.doctorName}>{doctor.nome_especialista}</Text>
            <Text style={styles.doctorSpecialty}>Cardiologista</Text>
            <Text style={styles.doctorCRM}>CRM {doctor.crm_especialista}</Text>
            <Text style={styles.doctorAddress}>
              <Icon name='location-on' color='#FFF' size={13}/>
              {doctor.logradouro_clinica}, {doctor.numero_clinica}
            </Text>
          </View>
        </View>
        <View style={styles.details}>
          { renderTabs(activeTab) }
          { renderDetailsBody(activeTab) }
        </View>
        <DetailsFooter buttonText='Selecionar Data' buttonPress={Actions.selectDate}/>
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
//  category: categoryDomain(),
//  locality: localityDomain(),
//  route: routeSelector(),
  activeTab: activeTabSelector()
})

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    openBiography: () => dispatch(openBiography()),
    openClinic: () => dispatch(openClinic())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DoctorDetails)
