import {
  OPEN_BIOGRAPHY,
  OPEN_CLINIC,
  OPEN_LIGHTROOM,
  CLOSE_LIGHTROOM
} from './constants'

export function openBiography () {
  return {
    type: OPEN_BIOGRAPHY,
    payload: {
      tab: 'BIOGRAPHY'
    }
  }
}

export function openClinic () {
  return {
    type: OPEN_CLINIC,
    payload: {
      tab: 'CLINIC'
    }
  }
}

export function openLightRoom (img) {
  return {
    type: OPEN_LIGHTROOM,
    payload: {
      img
    }
  }
}

export function closeLightRoom () {
  return {
    type: CLOSE_LIGHTROOM
  }
}
