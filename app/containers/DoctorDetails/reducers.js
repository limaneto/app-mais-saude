import { fromJS } from 'immutable'

import {
  OPEN_BIOGRAPHY,
  OPEN_CLINIC,
  OPEN_LIGHTROOM,
  CLOSE_LIGHTROOM
} from './constants'

const initialState = fromJS({
  doctor: {},
  tab: 'BIOGRAPHY',
  loading: {
    pending: false,
    fulfilled: false,
    rejected: false
  },
  lightRoom: {
    visible: false,
    img: null
  }
})

function doctorsListDetails (state = initialState, action) {
  const {type, payload} = action
  switch (type) {
    case OPEN_BIOGRAPHY:
      return state.set('tab', payload.tab)

    case OPEN_CLINIC:
      return state.set('tab', payload.tab)

    case OPEN_LIGHTROOM:
      return state.set('lightRoom', fromJS({
        visible: true,
        img: payload.img
      }))

    case CLOSE_LIGHTROOM:
      return state.set('lightRoom', fromJS({
        visible: false,
        img: null
      }))

    default:
      return state
  }
}

export default doctorsListDetails
