import React from 'react'
import { Image, Modal, Text, TouchableOpacity, View } from 'react-native'
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons"
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { lightRoomSelector } from './selectors'
import { closeLightRoom } from './actions'

import styles from './styles'

class LightRoom extends React.Component {
  render () {
    const { img, visible } = this.props.lightRoom
    const { closeModal } = this.props

    return (
      <Modal visible={visible}
        onRequestClose={closeModal}>
        <View style={styles.lightRoom.header}>
          <TouchableOpacity onPress={() => closeModal()} style={styles.lightRoom.closeBtn}>
            <MaterialCommunityIcon name='close' color='white' size={40} />
          </TouchableOpacity>
        </View>
        <View style={styles.lightRoom.wrapper}>
          <Image source={img} style={styles.lightRoom.image} resizeMode='contain'/>
        </View>
      </Modal>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  lightRoom: lightRoomSelector()
})

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    closeModal: () => dispatch(closeLightRoom())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LightRoom)
