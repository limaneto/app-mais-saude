import globalStyles from '../../global-styles'
export default {
  ...globalStyles,
  content: {
    ...globalStyles.content,
    backgroundColor: '#139E97',
    paddingHorizontal: 0,
    paddingTop: 45,
    paddingBottom: 0
  },
  header: {
    flex: 3,
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  doctorInfo: {
    flexDirection: 'column',
    flex: 1,
    height: 450,
    paddingHorizontal: 20,
    justifyContent: 'center'
  },
  doctorPhoto: {
    height: 100,
    width: 100
  },
  doctorName: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  },
  doctorSpecialty: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14
  },
  doctorCRM: {
    color: 'white',
    fontSize: 12
  },
  doctorAddress: {
    color: 'white',
    fontSize: 10
  },
  details: {
    backgroundColor: '#E9EBEE',
    flex: 6
  },
  tabWrapper: {
    height: 30,
    backgroundColor: '#139E97',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  tab: {
    flex: 1,
    alignSelf: 'stretch'
  },
  tabSelected: {
    flex: 1,
    alignSelf: 'stretch',
    borderBottomWidth: 2,
    borderBottomColor: 'white'
  },
  tabText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  tabBody: {
    flex: 9,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  footer: {
    flex: 1,
    height: 80,
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'space-between',
    alignContent: 'center',
    alignItems: 'stretch',
    paddingHorizontal: 25,
    paddingVertical: 15
  },
  priceText: {
    flex: 2,
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#139E97'
  },
  selectDateButton: {
    flexDirection: 'column',
    borderRadius: 99,
    flex: 4,
    backgroundColor: '#267DD3',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectDateBtnText: {
    color: 'white'
  },
  selectDateBtnIcon: {
    position: 'absolute',
    top: 15,
    right: 15
  },
  clinics: {
    thumbsView: {
      flexDirection: 'row',
      alignItems: 'flex-start',
      alignContent: 'stretch',
      justifyContent: 'space-between',
      flexWrap: 'wrap',
      paddingVertical: 20
    },
    thumb: {
      alignSelf: 'flex-start',
      width: 100,
      height: 60,
      marginRight: 5,
      marginBottom: 10
    }
  },
  lightRoom: {
    wrapper: {
      flex: 1,
      backgroundColor: '#000',
      justifyContent: 'center'
    },
    image: {
      flex: 1,
      width: null,
      height: null
    },
    header: {
      backgroundColor: '#000'
    },
    closeBtn: {
      alignSelf: 'flex-end'
    }
  }
}
