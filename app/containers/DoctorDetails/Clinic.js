import React from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { openLightRoom } from './actions'

import B from '../../components/B'

import styles from './styles'

class Clinic extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  render () {
    let gallery = [
      require('../../../etc/thumbnail/clinic_thumb_1.png'),
      require('../../../etc/thumbnail/clinic_thumb_2.png'),
      require('../../../etc/thumbnail/clinic_thumb_3.png'),
      require('../../../etc/thumbnail/clinic_thumb_4.png'),
      require('../../../etc/thumbnail/clinic_thumb_5.png'),
      require('../../../etc/thumbnail/clinic_thumb_6.png')
    ]

    const { openModal } = this.props
    return (
      <View style={styles.tabBody}>
        <B>Endereço da clínica</B>
        <Text>Av. Desembargador Moreira</Text>
        <View style={styles.clinics.thumbsView}>
          {gallery.map(
              (img, idx) => (
                <TouchableOpacity onPress={() => openModal(img)}>
                  <Image style={styles.clinics.thumb}
                  source={img} key={idx}/>
                </TouchableOpacity>
              )
          )}
        </View>
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({})

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    openModal: (img) => dispatch(openLightRoom(img))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Clinic)
