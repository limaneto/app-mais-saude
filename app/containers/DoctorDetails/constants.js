export const OPEN_BIOGRAPHY = 'app/DoctorDetails/OPEN_BIOGRAPHY'
export const OPEN_CLINIC = 'app/DoctorDetails/OPEN_CLINIC'

export const OPEN_LIGHTROOM = 'app/DoctorDetails/OPEN_LIGHTROOM'
export const CLOSE_LIGHTROOM = 'app/DoctorDetails/CLOSE_LIGHTROOM'
