import React from 'react'
import { Text, View } from 'react-native'

import B from '../../components/B'

import styles from './styles'

class Biography extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  render () {
    return (
      <View style={styles.tabBody}>
        <B>Biografia</B>
          <Text>Sobre a biografia</Text>
        <B>Formação Acadêmica</B>
          <Text>Sobre a formação acadêmica</Text>
      </View>
    )
  }
}

export default Biography
