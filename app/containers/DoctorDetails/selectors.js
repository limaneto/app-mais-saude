import { createSelector } from 'reselect'

/**
 * Direct selector to the doctorDetail state domain
 */
const doctorDetailsDomain = () => state => state.get('doctorDetails').toJS()

/**
 * Other specific selectors
 */
const routeSelector = () => state => state.get('route').toJS()

const activeTabSelector = () => state => state.get('doctorDetails').toJS().tab

const lightRoomSelector = () => state => state.get('doctorDetails').toJS().lightRoom
/**
 * Default selector used by DoctorDetails
 */
const makeDoctorDetails = () => createSelector(
  doctorDetailsDomain(),
  substate => substate.toJS()
)

export default makeDoctorDetails
export {
  routeSelector,
  doctorDetailsDomain,
  activeTabSelector,
  lightRoomSelector
}
