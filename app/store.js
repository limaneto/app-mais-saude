/**
 * Create the store with asynchronously loaded reducers
 */

import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-native-router-flux'
import promiseMiddleware from 'redux-promise-middleware'
import thunkMiddleware from 'redux-thunk'
import createReducer from './reducers'

export default function configureStore (initialState = {}) {
  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. promiseMiddleware: Handle promises
  const middlewares = [
    promiseMiddleware(),
    thunkMiddleware
  ]

  /* eslint-disable no-underscore-dangle */
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  const store = createStore(createReducer(), /* preloadedState, */ composeEnhancers(
    applyMiddleware(...middlewares)
  ))
  /*
  const store = compose(
    applyMiddleware(...middlewares)
  )(createStore)(createReducer())
  */
  // Extensions
  /*
  store.runSaga = sagaMiddleware.run
  store.asyncReducers = {} // Async reducer registry
  */

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  /*
    if (module.hot) {
      module.hot.accept('./reducers', () => {
        import('./reducers').then((reducerModule) => {
          const createReducers = reducerModule.default
          const nextReducers = createReducers(store.asyncReducers)

          store.replaceReducer(nextReducers)
        })
      })
    }
  */

  return store
}
