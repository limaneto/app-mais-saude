import { toUrlEncoded } from './utils/serialize'

const BASE_URL = 'http://app.maissaudeonline.com.br:8000/rest/'

export const api = {
  getEspelList: () => ({
    url: BASE_URL + 'espel_list',
    method: 'GET'
  }),
  getDoctorsList: () => ({
    url: BASE_URL + 'especialista',
    method: 'GET'
  }),
  getDoctorsListByEspel: (espel) => ({
    url: BASE_URL + 'especialista_list/' + espel,
    method: 'GET'
  })
}

export const googlePlacesAPI = {
  url: 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?',
  key: 'AIzaSyArm1QB6hYaMHmuoxNePLxPk5ctt88oCjY',
  get: function (options) {
    const queryString = toUrlEncoded(options)
    return {
      url: `${this.url}${queryString}&key=${this.key}`,
      method: 'GET'
    }
  }
}
