let source = {
  categories: [
    {name: 'Alergista'},
    {name: 'Andrologista'},
    {name: 'Anestesista'},
    {name: 'Arritmologista'},
    {name: 'Cardiologista'},
    {name: 'Cardiologista Pediátrico'},
    {name: 'Cirurgião Cardiovascular'},
    {name: 'Alergista'},
    {name: 'Andrologista'},
    {name: 'Anestesista'},
    {name: 'Arritmologista'},
    {name: 'Cardiologista'},
    {name: 'Cardiologista Pediátrico'},
    {name: 'Cirurgião Cardiovascular'},
    {name: 'Alergista'},
    {name: 'Andrologista'},
    {name: 'Anestesista'},
    {name: 'Arritmologista'},
    {name: 'Cardiologista'},
    {name: 'Cardiologista Pediátrico'},
    {name: 'Cirurgião Cardiovascular'}
  ],
  doctors: [
    {
      name: 'Dr. José Cauby Lima de Andrade Neto',
      category: 'Alergista',
      crm: '4270',
      crmuf: 'CE',
      address: 'Av Desembargador Moreira, 2001',
      price: 700.00
    },
    {
      name: 'Dr. José Cauby Lima de Andrade Neto',
      category: 'Alergista',
      crm: '4270',
      crmuf: 'CE',
      address: 'Av Desembargador Moreira, 2001',
      price: 700.00
    },
    {
      name: 'Dr. José Cauby Lima de Andrade Neto',
      category: 'Alergista',
      crm: '4270',
      crmuf: 'CE',
      address: 'Av Desembargador Moreira, 2001',
      price: 700.00
    },
    {
      name: 'Dr. José Cauby Lima de Andrade Neto',
      category: 'Alergista',
      crm: '4270',
      crmuf: 'CE',
      address: 'Av Desembargador Moreira, 2001',
      price: 700.00
    },
    {
      name: 'Dr. José Cauby Lima de Andrade Neto',
      category: 'Alergista',
      crm: '4270',
      crmuf: 'CE',
      address: 'Av Desembargador Moreira, 2001',
      price: 700.00
    }
  ]
}

export default source
