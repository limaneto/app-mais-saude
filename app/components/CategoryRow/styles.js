export default {
  container: {
    flex: 1,
    paddingRight: 1,
    paddingTop: 13,
    paddingBottom: 13,
    borderBottomWidth: 0.5,
    borderColor: '#c9c9c9',
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    marginLeft: 12,
    fontSize: 16,
    fontWeight: 'bold'
  }
}
