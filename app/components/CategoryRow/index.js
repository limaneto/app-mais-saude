import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import styles from './styles'

const CategoryRow = props => (
  <TouchableOpacity style={styles.container} {...props}>
    <Text style={styles.text}>
      {props.nome_espel}
    </Text>
  </TouchableOpacity>
)

export default CategoryRow
