import React from 'react'
import { TouchableOpacity } from 'react-native'
import styled from 'styled-components/native'

const Image = styled.Image`
  width: 100%;
  justify-content: center;
  align-items: center;
  align-self: flex-start;
  flex: 1;
`

export default (props) => (
  <TouchableOpacity {...props}>
    <Image borderRadius={25} style={props.innerStyle}
      source={require('../../../images/assets/gradientButtonBlueBg.png')}>
      { props.children }
    </Image>
  </TouchableOpacity>
)
