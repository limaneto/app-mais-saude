import React from 'react'
import {
  Text
} from 'react-native'
import styled from 'styled-components/native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const TabFooter = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  height: 60;
  border-top-width: 1;
  border-top-color: #0D8D87;
  elevation: 10;
  background-color: #FFF;
`

const FooterButton = styled.TouchableOpacity`
  align-items: center;
`

export default props => (
  <TabFooter>
    <FooterButton>
      <Icon name='calendar' size={25} color='black' color={'#0D8D87'} />
      <Text>Agendar</Text>
    </FooterButton>
    <FooterButton>
      <Icon name='checkbox-marked-outline' size={25} color='black' />
      <Text>Consultas e Exames</Text>
    </FooterButton>
    <FooterButton>
      <Icon name='account-outline' size={25} color='black' />
      <Text>Conta</Text>
    </FooterButton>
  </TabFooter>
)
