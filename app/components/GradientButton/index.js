import React from 'react'
import styled from 'styled-components/native'

const Image = styled.Image`
  width: 100%;
  align-items: center;
  align-self: stretch;
  resize-mode: cover;
  padding-top: 15;
`

const GradientButton = styled.TouchableOpacity`
  align-items: center;
  align-self: stretch;
  margin-top: 20;
  margin-bottom: -20;
`

export default (props) => (
  <GradientButton {...props}>
    <Image borderRadius={25} height={52}
      source={require('../../../images/assets/gradientButtonBg.png')}>
      { props.children }
    </Image>
  </GradientButton>
)
