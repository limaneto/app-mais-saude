import React from 'react'
import { Icon } from 'native-base'
import styled from 'styled-components/native'

const Input = styled.TextInput`
  background-color: rgba(0, 0, 0, 0);
  border-bottom-color: #36A4A1;
  align-self: center;
  width: 100%;
`

const View = styled.View`
  flex-direction: row;
  align-items: flex-end;
  margin-bottom: 5;
`

const iconStyles = {
  width: 25,
  marginLeft: 5,
  marginRight: 5,
  marginBottom: 10,
  paddingTop: 10,
  color: '#6C6C6C',
  textAlign: 'center'
}

export default (props) => (
  <View style={{borderBottomColor: '#36A4A1', borderBottomWidth: 2}}>
    { props.icon ? <Icon name={props.icon} style={iconStyles}/> : null }
    <Input multiline={false}  underlineColorAndroid='transparent' {...props}>
    </Input>
  </View>
)
