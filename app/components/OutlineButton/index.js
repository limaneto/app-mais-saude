import styled from 'styled-components/native'

export default styled.TouchableOpacity`
  align-items: center;
  align-self: center;
  justify-content: center;
  margin: 10 0 0 0;
  border-radius: 25;
  border-width: 2;
  border-color: #A7A7A7;
  height: 42;
  width: 200;
`
