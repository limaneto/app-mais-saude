import {
  OPEN_SEARCH_INPUT
} from './constants'

export function toggleSearchInput (mode) {
  return {
    type: OPEN_SEARCH_INPUT,
    payload: !!mode
  }
}
