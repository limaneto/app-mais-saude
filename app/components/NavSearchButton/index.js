import React from 'react'
import styled from 'styled-components/native'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { routeSelector } from './selectors'
import { toggleSearchInput } from './actions'

const NavButtonsView = styled.View`
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  width: 50;
`

const iconStyle = {
  paddingRight: 15
}

class NavSearchButton extends React.Component {
  handleSearchBtnPress () {
    const { search } = this.props.route
    this.props.toggleSearch(!search.on)
  }

  render () {
    const handleSearchBtnPress = this.handleSearchBtnPress.bind(this)

    return (
      <NavButtonsView>
        <TouchableOpacity onPress={handleSearchBtnPress}>
          <MaterialIcon name='search' size={28} color='white' style={iconStyle}/>
        </TouchableOpacity>
        <MaterialCommunityIcon name='bell-outline' size={28} color='white' style={iconStyle}/>
      </NavButtonsView>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  route: routeSelector()
})

const mapDispatchToProps = dispatch => ({
  dispatch,
  toggleSearch: mode => dispatch(toggleSearchInput(mode))
})

export default connect(mapStateToProps, mapDispatchToProps)(NavSearchButton)
