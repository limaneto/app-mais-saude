const routeSelector = () => state => state.get('route').toJS()

export {
  routeSelector
}
