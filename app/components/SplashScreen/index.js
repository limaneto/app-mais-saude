import React from 'react'
import { StatusBar, View } from 'react-native'
import { Actions } from 'react-native-router-flux'
import Logo from '../Logo'
import styles from './styles'

export default class SplashScreen extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      Actions.firstRun()
    }, 1500)
  }

  render() {
    return (
      <View style={styles.content}>
        <StatusBar backgroundColor='white' barStyle='dark-content'/>
        <Logo source={require('../../../images/logo.png')}/>
      </View>
    )
  }
}
