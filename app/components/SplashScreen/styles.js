import global from '../../global-styles.js'

export default {
  ...global,
  content: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center'
  }
}
